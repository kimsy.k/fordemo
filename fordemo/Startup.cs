﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(fordemo.Startup))]
namespace fordemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
